import React from 'react';
import './App.css';
import 'primereact/resources/themes/nova-light/theme.css';
import 'primeflex/primeflex.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import { Menubar } from "primereact/menubar";
import { InputText } from "primereact/inputtext";
import { Fieldset } from "primereact/fieldset";
import { InputTextarea } from "primereact/inputtextarea";

const entities = [
    { key: '#', name: 'location' },
    { key: '@', name: 'characters' },
    { key: '!', name: 'items' }
];

const menuBarItems = [
    { label: 'Home', icon: 'pi pi-fw pi-home' },
    {
        label: 'Entities',
        icon: 'pi pi-fw pi-list',
        items: entities.map( ({key, name}) => ({ label: `${key} ${name}`, items: []}))
    }
];


const logItems = [
    { text: '@Kurtis@ been hit by a @drow@.' },
    { text: '@Rick@ has bought a !longsword! in #Stadford#.' }
];

function App() {
    return (
        <div>
            <div className="p-grid p-dir-col">
            <Menubar model={menuBarItems} className="p-col">
                <InputText placeholder="Search" type="text"/>
            </Menubar>
            </div>
            <div className="p-col">
                <Fieldset legend="Add log">
                    <InputTextarea style={ { width: '100%' }} autoResize={true} />
                </Fieldset>
            </div>
            { logItems.map( ({ text }) => <div className="p-col">{text}</div> )}
        </div>
    );
}

export default App;
